/**
 * 
 */
package com.alibaba.druid.sql;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.util.JdbcUtils;

/**
 * @author zhaowen.zhuang
 * @Date Feb 5, 2015
 */
public class SQLUtilsTest {

    /**
     * Test method for
     * {@link com.alibaba.druid.sql.SQLUtils#parseStatements(java.lang.String, java.lang.String)}.
     */
    @Test
    public void testParseStatements() {
        List<SQLStatement> statements =
                SQLUtils.parseStatements("select * from tdp_app where id = 1 and name = '123'", JdbcUtils.MYSQL);
        System.out.println(statements);
    }

}
