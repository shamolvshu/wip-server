/**
 * 
 */
package com.tcl.wip.client.repository;

import java.sql.Types;
import java.util.Date;

import com.tcl.wip.client.annotation.CreatedTime;
import com.tcl.wip.client.annotation.GenerationStrategy;
import com.tcl.wip.client.annotation.Id;
import com.tcl.wip.client.annotation.ModifiedTime;
import com.tcl.wip.client.annotation.ShardingKey;
import com.tcl.wip.client.annotation.Table;
import com.tcl.wip.client.query.Column;

/**
 * @author zhaowen.zhuang
 * @Date Jan 19, 2015
 */
@Table("test_entity")
public class TestEntity {

    public static final Column<Long> ID = new Column<>("id", Long.class, Types.BIGINT);

    public static final Column<String> NAME = new Column<>("name", String.class, Types.VARCHAR);

    @Id(generationStrategy = GenerationStrategy.WIP_SEQUENCE)
    @ShardingKey
    private Long id;

    private String name;

    private String nickName;

    @CreatedTime
    private Date createdTime;

    @ModifiedTime
    private Date modifiedTime;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "TestEntity [id=" + id + ", name=" + name + ", nickName=" + nickName + "]";
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }



}
