/**
 * 
 */
package com.tcl.wip.client.repository;

import static com.tcl.wip.client.repository.TestEntity.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author zhaowen.zhuang
 * @Date Jan 19, 2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class TestEntityRepositoryTest {

    @Autowired
    private TestEntityRepository testEntityRepository;


    @Test
    public void testInsert() {
        for (int i = 0; i < 1000; i++) {
            TestEntity entity = new TestEntity();
            entity.setName("hello");
            entity.setNickName("hello world");
            testEntityRepository.insert(entity);
        }

    }

    @Test
    public void testUpdate() {
        TestEntity entity = new TestEntity();
        entity.setId(29200L);
        entity.setName("hello xxxx");
        entity.setNickName("hello world xxxx");
        testEntityRepository.update(entity);

        TestEntity result = testEntityRepository.withShardingKey(29200L).singleResult();
        System.out.println(result);
    }

    //
    // @Test
    // public void testDelete() {
    // testEntityRepository.delete("1", "2");
    // }
    //
    @Test
    public void testSelectList() {
        List<TestEntity> list =
                testEntityRepository.withShardingKey(34502L).and(ID.eq(34502L)).or(NAME.like("%"))
                        .orderBy(ID.asc(), NAME.desc()).list();
        System.out.println(list);
    }

    @Test
    public void testCount() {
        int count =
                testEntityRepository.withShardingKey(34502L).and(ID.eq(34502L)).or(NAME.like("%"))
                        .count();
        System.out.println(count);
    }

    @Test
    public void testDeleteList() {
        int deleteCount =
                testEntityRepository.withShardingKey(123L).and(ID.eq(123L)).or(NAME.eq("123"))
                        .delete();
        System.out.println(deleteCount);
    }

    @Test
    public void testListPage() {
        List<TestEntity> listPage =
                testEntityRepository.withShardingKey(123L).and(ID.eq(123L)).or(NAME.eq("123"))
                        .listPage(0, 10);// limit 0, 10
    }


}
