/**
 * 
 */
package com.tcl.wip.client.repository;


/**
 * @author zhaowen.zhuang
 * @Date Jan 19, 2015
 */
public interface TestEntityRepository extends WipRepository<TestEntity, Long, Long> {

}
