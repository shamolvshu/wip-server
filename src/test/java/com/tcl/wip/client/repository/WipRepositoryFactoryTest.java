/**
 * 
 */
package com.tcl.wip.client.repository;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
public class WipRepositoryFactoryTest {

    /**
     * Test method for
     * {@link com.tcl.wip.client.repository.WipRepositoryFactory#createRepository(java.lang.Class, com.tcl.wip.client.repository.DatasourceManager)}
     * .
     */
    @Test
    public void testCreateRepository() {
        TestEntityRepository repository =
                new WipRepositoryFactory().createRepository(TestEntityRepository.class, null);
        assertNotNull(repository);
        System.out.println(repository.withShardingKey(123L).toSql());
    }

}
