/**
 * 
 */
package com.tcl.wip.client.util;

import static org.junit.Assert.*;

import java.lang.reflect.Type;

import org.junit.Test;

import com.tcl.wip.client.common.utils.ClassUtils;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
public class ClassUtilsTest {

    @Test
    public void testGetInterfaceGenricType() {
        Type[] types = ClassUtils.getInterfaceGenricType(SubInterface.class, Interface.class);
        assertEquals(Integer.class, types[0]);
    }

    interface Interface<T> {}

    interface SubInterface extends Interface<Integer> {};

}
