/**
 * 
 */
package com.tcl.wip.client.util;

import com.alibaba.druid.sql.PagerUtils;
import com.alibaba.druid.util.JdbcUtils;

/**
 * @author zhaowen.zhuang
 * @Date Feb 5, 2015
 */
public class PaperUtilTest {

    public static void main(String[] args) {
        System.out.println(PagerUtils.limit("select * from tdp where id = ? and name = ?", JdbcUtils.MYSQL, 2, 10));
    }
}
