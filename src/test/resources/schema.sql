CREATE TABLE `test_entity` (
  id varchar(64),
  name varchar(64),
  nick_name varchar(64)
) ;

insert into test_entity(id, name,nick_name ) values('123', 'first one', 'here is nickname');
insert into test_entity(id, name,nick_name ) values('1234', 'second one', 'nick, nick');
