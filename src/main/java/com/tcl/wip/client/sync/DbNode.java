package com.tcl.wip.client.sync;

import java.io.Serializable;

import com.tcl.wip.commons.constants.wip.MaintainStatus;

/**
 * 数据库配置
 * Created by long.hua on 2015/1/14.
 */
public class DbNode implements Serializable {

    private Long id;

    private String ip;

    private String port;

    private String name;

    private String username;

    private String password;

    private Long hashCode;

    private String maintainStatus;

    private String createTime;

    private String updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getHashCode() {
        return hashCode;
    }

    public void setHashCode(Long hashCode) {
        this.hashCode = hashCode;
    }

    public MaintainStatus getMaintained() {
        try{
            return MaintainStatus.valueOf(maintainStatus);
        }catch (Exception e){
            return null;
        }
    }

    public void setMaintained(String maintainStatus) {
            this.maintainStatus=maintainStatus;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

}
