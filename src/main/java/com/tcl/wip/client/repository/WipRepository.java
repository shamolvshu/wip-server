/**
 * 
 */
package com.tcl.wip.client.repository;

import com.tcl.wip.client.query.QueryBuilder;

/**
 * 
 * @author zhaowen.zhuang
 *
 * @param <E> Entity Type
 * @param <ID> Entity Id Type
 * @param <SK> Sharding Key Type
 */
public interface WipRepository<E, ID, SK> {

    boolean insert(E entity);

    /**
     * 所有字段都会被更新， 即使 null ， 建议使用 select 出来的对象来更新。
     * 
     * @param entity
     */
    boolean update(E entity);

    boolean delete(ID id, SK shardKey);

    /**
     * @param sk
     * @return
     */
    QueryBuilder<E, SK> withShardingKey(SK sk);


}
