/**
 *
 */
package com.tcl.wip.client.repository;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import com.tcl.wip.client.orm.IdGenerator;

/**
 * @author zhaowen.zhuang
 * @Date Jan 19, 2015
 */
public class EntityMeta {

    private Class<?> entityType;

    private String tableName;

    private Field idField;

    private Field shardingKeyField;

    private Map<String, String> columnToPropertyMap;

    private List<Field> propertyFields;

    private IdGenerator<?> idGenerator;

    public Class<?> getEntityType() {
        return entityType;
    }

    public void setEntityType(Class<?> entityType) {
        this.entityType = entityType;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Map<String, String> getColumnToPropertyMap() {
        return columnToPropertyMap;
    }

    public void setColumnToPropertyMap(Map<String, String> columnToPropertyMap) {
        this.columnToPropertyMap = columnToPropertyMap;
    }

    public void setPropertyFields(List<Field> propertyFields) {
        this.propertyFields = propertyFields;
    }

    public List<Field> getPropertyFields() {
        return propertyFields;
    }

    public Field getIdField() {
        return idField;
    }

    public void setIdField(Field idField) {
        this.idField = idField;
    }

    public Field getShardingKeyField() {
        return shardingKeyField;
    }

    public void setShardingKeyField(Field shardingKeyField) {
        this.shardingKeyField = shardingKeyField;
    }

    public IdGenerator<?> getIdGenerator() {
        return idGenerator;
    }

    public void setIdGenerator(IdGenerator<?> idGenerator) {
        this.idGenerator = idGenerator;
    }
}
