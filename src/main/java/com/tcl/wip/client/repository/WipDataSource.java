package com.tcl.wip.client.repository;

import com.alibaba.druid.pool.DruidDataSource;
import com.tcl.wip.commons.constants.wip.MaintainStatus;

/**
 * Created by long.hua on 2015/1/22.
 */
public class WipDataSource extends DruidDataSource{

    private volatile MaintainStatus maintainStatus=MaintainStatus.Normal;

    public MaintainStatus getMaintainStatus() {
        return maintainStatus;
    }

    public void setMaintainStatus(MaintainStatus maintainStatus) {
        this.maintainStatus = maintainStatus;
    }
}
