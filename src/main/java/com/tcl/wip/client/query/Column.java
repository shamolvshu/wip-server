/**
 * 
 */
package com.tcl.wip.client.query;

/**
 * @author zhaowen.zhuang
 *
 */
public class Column<T> {


    /**
     * @param name
     * @param javaType
     * @param jdbcType {@link java.sql.Types}
     */
    public Column(String name, Class<T> javaType, int jdbcType) {
        super();
        this.name = name;
        this.javaType = javaType;
        this.jdbcType = jdbcType;
    }

    private String name;

    private Class<T> javaType;

    private int jdbcType;

    public String getName() {
        return name;
    }


    public Class<?> getJavaType() {
        return javaType;
    }

    public int getJdbcType() {
        return jdbcType;
    }

    public Expression eq(T value) {
        return new SimpleExpression<T>(this.name, "%s = ?", new Parameter<T>(value, this.jdbcType));
    }

    public Expression le(T value) {
        return new SimpleExpression<T>(this.name, "%s <= ?", new Parameter<T>(value, this.jdbcType));
    }

    public Expression ge(T value) {
        return new SimpleExpression<T>(this.name, "%s >= ?", new Parameter<T>(value, this.jdbcType));
    }

    public Expression lt(T value) {
        return new SimpleExpression<T>(this.name, "%s < ?", new Parameter<T>(value, this.jdbcType));
    }

    public Expression gt(T value) {
        return new SimpleExpression<T>(this.name, "%s > ?", new Parameter<T>(value, this.jdbcType));
    }

    public Expression like(T value) {
        return new SimpleExpression<T>(this.name, "%s like ?", new Parameter<T>(value,
                this.jdbcType));
    }

    public Expression between(T lowValue, T heightValue) {
        return new BetweenExpression<T>(this.name, new Parameter<T>(lowValue, this.jdbcType),
                new Parameter<T>(heightValue, this.jdbcType));
    }

    public OrderExpression desc() {
        return new OrderExpression(this.name, OrderExpression.DESC);
    }

    public OrderExpression asc() {
        return new OrderExpression(this.name, OrderExpression.ASC);
    }

}
