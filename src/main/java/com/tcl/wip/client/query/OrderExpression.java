/**
 * 
 */
package com.tcl.wip.client.query;

import java.util.List;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
public class OrderExpression extends Expression {

    public static final String DESC = " desc";

    public static final String ASC = " asc";



    /**
     * @param column
     * @param direction
     */
    public OrderExpression(String column, String direction) {
        this.column = column;
        this.direction = direction;
    }

    private String column;

    private String direction;

    @Override
    String toSqlFragment() {
        return column + direction;
    }

    @Override
    void appendParameterTo(List<Parameter<?>> parameters) {
        // there is no parameter in order expression
        return;
    }

}
