/**
 * 
 */
package com.tcl.wip.client.query;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
public class Restriction {

    public static final String OR = " or ";

    public static final String AND = " and ";



    public Restriction(String op, Expression expression) {
        this.op = op;
        this.expression = expression;
    }

    private String op;

    private Expression expression;

    public String getOp() {
        return op;
    }

    public Expression getExpression() {
        return expression;
    }


}
