/**
 * 
 */
package com.tcl.wip.client.query;

import java.util.List;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
public class BetweenExpression<T> extends Expression {

    /**
     * @param column
     * @param lowValue
     * @param heightValue
     */
    public BetweenExpression(String column, Parameter<T> lowValue, Parameter<T> heightValue) {
        this.column = column;
        this.lowValue = lowValue;
        this.heightValue = heightValue;
    }

    private String column;

    private Parameter<T> lowValue;

    private Parameter<T> heightValue;

    @Override
    String toSqlFragment() {
        return column + " between ? and ?";
    }

    @Override
    void appendParameterTo(List<Parameter<?>> parameters) {
        parameters.add(lowValue);
        parameters.add(heightValue);

    }


}
