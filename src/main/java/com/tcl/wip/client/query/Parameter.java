/**
 * 
 */
package com.tcl.wip.client.query;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
/**
 * @author zhaowen.zhuang
 * @Date Jan 22, 2015
 * @param <T>
 */
public class Parameter<T> {

    public Parameter(T value, int jdbcType) {
        this.value = value;
        this.jdbcType = jdbcType;
    }

    private int jdbcType;

    private T value;

    public T getValue() {
        return value;
    }


    public int getJdbcType() {
        return jdbcType;
    }


    @Override
    public String toString() {
        return "Parameter [jdbcType=" + jdbcType + ", value=" + value + "]";
    }



}
