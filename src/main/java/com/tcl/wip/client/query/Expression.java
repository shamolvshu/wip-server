/**
 * 
 */
package com.tcl.wip.client.query;

import java.util.List;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
public abstract class Expression {

    abstract String toSqlFragment();

    abstract void appendParameterTo(List<Parameter<?>> parameters);

}
