/**
 * 
 */
package com.tcl.wip.client.query;

import java.util.List;

/**
 * 
 * @author zhaowen.zhuang
 * @Date Jan 19, 2015
 * @param <T>
 */
public class SimpleExpression<T> extends Expression {

    private String pattern;

    private String column;

    private Parameter<T> parameter;



    /**
     * @param pattern
     * @param column
     * @param parameter
     */
    public SimpleExpression(String column, String pattern, Parameter<T> parameter) {
        this.pattern = pattern;
        this.column = column;
        this.parameter = parameter;
    }

    public String getColumn() {
        return column;
    }

    public Parameter<T> getParameter() {
        return parameter;
    }

    @Override
    public String toSqlFragment() {
        return String.format(this.pattern, this.column);
    }

    @Override
    public String toString() {
        return "[" + toSqlFragment() + "] with arguments :" + parameter;
    }

    @Override
    void appendParameterTo(List<Parameter<?>> parameters) {
        parameters.add(parameter);
    }
}
