/**
 * 
 */
package com.tcl.wip.client.query;

import java.util.List;

/**
 * @author zhaowen.zhuang
 * @Date Jan 21, 2015
 */
public class Query<SK> {


    Query(SK shardingKey, String where, List<Parameter<?>> parameters) {
        this.where = where;
        this.parameters = parameters;
        this.shardingKey = shardingKey;
    }

    String where;

    List<Parameter<?>> parameters;

    private SK shardingKey;

    public String getWhere() {
        return where;
    }

    public List<Parameter<?>> getParameters() {
        return parameters;
    }

    public SK getShardingKey() {
        return shardingKey;
    }


}
