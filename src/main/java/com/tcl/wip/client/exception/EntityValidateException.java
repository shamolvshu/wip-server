/**
 * 
 */
package com.tcl.wip.client.exception;

/**
 * @author zhaowen.zhuang
 * @Date Jan 23, 2015
 */
public class EntityValidateException extends WipException {

    private static final long serialVersionUID = -3545478154581689088L;

    public EntityValidateException() {
        super();
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public EntityValidateException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public EntityValidateException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public EntityValidateException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public EntityValidateException(Throwable cause) {
        super(cause);
    }


}
