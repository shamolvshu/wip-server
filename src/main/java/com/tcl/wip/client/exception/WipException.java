/**
 * 
 */
package com.tcl.wip.client.exception;

/**
 * @author zhaowen.zhuang
 * @Date Jan 21, 2015
 */
public class WipException extends RuntimeException {

    private static final long serialVersionUID = 3832015215140484818L;

    /**
     * 
     */
    public WipException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public WipException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public WipException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public WipException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public WipException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }



}
