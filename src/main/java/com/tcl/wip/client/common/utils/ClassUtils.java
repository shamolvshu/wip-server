/**
 * 
 */
package com.tcl.wip.client.common.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
public class ClassUtils {

    public static Type[] getInterfaceGenricType(Class<?> clazz, Class<?> interfacee) {

        Type[] genType = clazz.getGenericInterfaces();
        for (Type type : genType) {
            if (type instanceof ParameterizedType) {
                ParameterizedType ptype = (ParameterizedType) type;
                if (ptype.getRawType().equals(interfacee)) {
                    return ptype.getActualTypeArguments();
                }
            }
        }
        return null;
    }
}
