/**
 * 
 */
package com.tcl.wip.client.annotation;

/**
 * @author zhaowen.zhuang
 * @Date Jan 23, 2015
 */
public enum GenerationStrategy {

    /**
     * 开发者自己生成
     */
    DEFAULT,

    /**
     * 使用随机UUDI做为主键
     */
    UUID,

    /**
     * 使用服务器端分配的 sequence
     */
    WIP_SEQUENCE

}
