/**
 * 
 */
package com.tcl.wip.client.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhaowen.zhuang
 * @Date Jan 22, 2015
 */
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

    /**
     * 列名
     * 
     * @return
     */
    String value();

}
