/**
 * 
 */
package com.tcl.wip.client.spring.config;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * @author zhaowen.zhuang
 * @Date Jan 20, 2015
 */
public class WipNamespaceHandler extends NamespaceHandlerSupport {

    @Override
    public void init() {
        registerBeanDefinitionParser("app", new WipAppBeanDefinitionParser());
    }

}
