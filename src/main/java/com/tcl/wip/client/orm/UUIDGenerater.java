/**
 * 
 */
package com.tcl.wip.client.orm;

import java.util.UUID;

/**
 * @author zhaowen.zhuang
 * @Date Jan 23, 2015
 */
public class UUIDGenerater implements IdGenerator<String> {

    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }

}
