/**
 * 
 */
package com.tcl.wip.client.orm;

/**
 * @author zhaowen.zhuang
 * @Date Jan 23, 2015
 */
public interface IdGenerator<T> {

    T generate();
}
