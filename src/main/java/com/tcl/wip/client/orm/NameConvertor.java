/**
 * 
 */
package com.tcl.wip.client.orm;

import com.google.common.base.CaseFormat;

/**
 * @author zhaowen.zhuang
 * @Date Jan 22, 2015
 */
public class NameConvertor {

    public static String toColumnName(String s) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, s);
    }

    public static String toTableName(String s) {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, s);
    }
}
